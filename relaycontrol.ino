////////////////////////////////////////////////////////////////////////////////
// RELAYControl
////////////////////////////////////////////////////////////////////////////////




////////////////////////////////////////////////////////////////////////////////
// Libraries used
////////////////////////////////////////////////////////////////////////////////

#define XCONTROL_NAME F("RELAY1")              // application name
#define XCONTROL_VER  F("v1.1.0")              // application version

//#include <SPI.h>                             // Arduino Ethernet/SPI lib
//#include <Ethernet.h>
#include <UIPEthernet.h>                       // UIP Ethernet library



////////////////////////////////////////////////////////////////////////////////
// Circuit setup
////////////////////////////////////////////////////////////////////////////////

// relay configuration
const byte relayMode  PROGMEM    = 1;      // (1=GPIO pins; 2=Shift register)
const byte relayCount PROGMEM    = 12 ;    // number of relay pins used
const byte relayPins[]           = {       // GPIO pins used to control relays
  A4, A3, A2, A1, A0, 3, 4, 5, 6, 7, 8, 9
};
const char* relayNames[] = {               // relay display naming
  "Pendu",
  "Bureau",
  "Desactiver",
  "Chaines",
  "DVD",
  "Chambre",
  "Croix",
  "UV",
  "Alarme",
  "Strobe",
  "Eclairage",
  "Fume"
};
const char relayType[]           = {       // relay function type
  's', 's', 's', 's',
  's', 's', 's', 's',
  's', 's', 's', 's'
};
boolean const relayInvert        = true;
int const     relayPulseLength   = 1000;   // pulse length in milliseconds
int const     relayBuzzLength    = 80;     // buzz length in milliseconds
int const     relayBuzzCount     = 20;     // buzz number of periods

// menu configuration
const byte menuCount PROGMEM     = 1;      // number of menu items
const String menuNames[]         = {       // menu display naming
  "Relay 1"
};
const String menuLinks[]         = {       // menu href links
  "http://192.168.43.177"
};

// network configuration
byte netMAC[]                    = {       // mac address
  0xDE, 0xAD, 0xB0,
  0xEF, 0xFD, 0x17
};
byte netIP[]                     = {       // ip address
  192, 168, 15, 177
};




////////////////////////////////////////////////////////////////////////////////
// Runtime variables
////////////////////////////////////////////////////////////////////////////////

//ethernet module setup
EthernetServer netServer(80);

//relay status variables
byte relayState[]                = {       // current state (on=1, off=0)
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};
unsigned long relayUpdate[]      = {       // last updated ts (using millis)
  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};



////////////////////////////////////////////////////////////////////////////////
// Setup / main section
////////////////////////////////////////////////////////////////////////////////

// setup routine
void setup() {
  if (relayMode == 1) {
    for (byte i = 0; i < relayCount; i++) {
      pinMode(relayPins[i], OUTPUT);
    }
  } else {
// shift mode was here   
  }
  for (byte i = 0; i < relayCount; i++) {
    if(relayInvert) {
      digitalWrite(relayPins[i], HIGH);
    } else {
      digitalWrite(relayPins[i], LOW);
    }
    relayState[i] = 0;
  }
  Ethernet.begin(netMAC, netIP);
  netServer.begin();
}

// main loop
void loop() {
  interruptHttp();
  delay(10);
}

// io function
void io(byte pin, byte state) {
  relayState[pin]  = 1;
  relayUpdate[pin] = millis();
  if (relayMode==1) {
    if(relayInvert) {
      digitalWrite(relayPins[pin], LOW);
    } else {
      digitalWrite(relayPins[pin], HIGH);
    }
  } else {
// shift mode was here
  }
}



////////////////////////////////////////////////////////////////////////////////
// HTTP and Serial request processing
////////////////////////////////////////////////////////////////////////////////

// main http processing
void httpProcess(EthernetClient ec, String url) {
  String url2 = url;
  if (url == "" or url == "/") {
    htmlHeader(ec);
    htmlMenu(ec);
    htmlRelay(ec);
    htmlFooter(ec);
  } else if (url.startsWith("/switch")) {
    url.replace(F("/switch"), "");
    if (url.startsWith("/on")) {
      url.replace(F("/on/"), "");
      if(relayInvert) {
        digitalWrite(relayPins[url.toInt()], LOW);
      } else {
        digitalWrite(relayPins[url.toInt()], HIGH);
      }
      relayState[url.toInt()] = 1;
      relayUpdate[url.toInt()] = millis();
      jsonResponse(ec, url.toInt(), F("ok"));
    } else if (url.startsWith("/off")) {
      url.replace(F("/off/"), "");
      if(relayInvert) {
        digitalWrite(relayPins[url.toInt()], HIGH);
      } else {
        digitalWrite(relayPins[url.toInt()], LOW);
      }
      relayState[url.toInt()] = 0;
      relayUpdate[url.toInt()] = millis();
      jsonResponse(ec, url.toInt(), F("ok"));
    } else if (url.startsWith("/toggle")) {
      url.replace(F("/toggle/"), "");
      if(relayState[url.toInt()] == 0) {
        if(relayInvert) {
          digitalWrite(relayPins[url.toInt()], LOW);
        } else {
          digitalWrite(relayPins[url.toInt()], HIGH);
        }
        relayState[url.toInt()] = 1;
      } else {
        if(relayInvert) {
          digitalWrite(relayPins[url.toInt()], HIGH);
        } else {
          digitalWrite(relayPins[url.toInt()], LOW);
        }
        relayState[url.toInt()] = 0;
      }
      relayUpdate[url.toInt()] = millis();
      jsonResponse(ec, url.toInt(), F("ok"));
    }
  } else if (url.startsWith("/pulse")) {
    url.replace(F("/pulse/"), "");
    if(relayInvert) {
      digitalWrite(relayPins[url.toInt()], LOW);
    } else {
      digitalWrite(relayPins[url.toInt()], HIGH);
    }
    delay(relayPulseLength);
    if(relayInvert) {
      digitalWrite(relayPins[url.toInt()], HIGH);
    } else {
      digitalWrite(relayPins[url.toInt()], LOW);
    }
    relayUpdate[url.toInt()] = millis();
    jsonResponse(ec, url.toInt(), F("ok"));
  } else if (url.startsWith("/buzz")) {
    url.replace(F("/buzz/"), "");
    for (int i=0; i<relayBuzzCount; i++) {
      if(relayInvert) {
        digitalWrite(relayPins[url.toInt()], LOW);
      } else {
        digitalWrite(relayPins[url.toInt()], HIGH);
      }
      delay(relayBuzzLength/2);
      if(relayInvert) {
        digitalWrite(relayPins[url.toInt()], HIGH);
      } else {
        digitalWrite(relayPins[url.toInt()], LOW);
      }
      delay(relayBuzzLength/2);
    }
    relayUpdate[url.toInt()] = millis();
    jsonResponse(ec, url.toInt(), F("ok"));
  } else if (url.startsWith("/css")) {
    htmlCSS(ec);
  } else if (url.startsWith("/js/common")) {
    htmlJSCommon(ec);
  } else if (url.startsWith("/js/custom")) {
    htmlJSCustom(ec);
  }
}

// relay list display
void htmlRelay(EthernetClient ec) {
  for (byte i = 0; i < relayCount; i++) {
    ec.println(F("  <card>"));
    ec.print  (F("    <name>"));
    ec.print  (relayNames[i]);
    ec.println(F("    </name>"));
    ec.println(F("    <action>"));
    ec.print(F("     updated: "));
    if (relayUpdate[i] == 0) {
      ec.println(F("never"));
    } else if (millis() - relayUpdate[i] < 5000) {
      ec.println(F("just now"));
    } else if (millis() - relayUpdate[i] < 60000) {
      ec.print(long((millis() - relayUpdate[i]) / 1000)); ec.println(F("s ago"));
    } else if (millis() - relayUpdate[i] < 3600000) {
      ec.print(long((millis() - relayUpdate[i]) / 60000)); ec.println(F("m ago"));
    } else {
      ec.print(long((millis() - relayUpdate[i]) / 3600000)); ec.println(F("h ago"));
    }
    switch (relayType[i]) {
      case 's':
        ec.print(F("      <state id=\"state")); ec.print(i); ec.print(F("on\" "));  ec.print(relayState[i] == 1 ? "class=\"active\" " : ""); ec.print(F("onclick=\"xcRelaySwitch(")); ec.print(i); ec.println(F(", 'on');\"/>ON</state>"));
        ec.print(F("      <state id=\"state")); ec.print(i); ec.print(F("off\" ")); ec.print(relayState[i] == 0 ? "class=\"active\" " : ""); ec.print(F("onclick=\"xcRelaySwitch(")); ec.print(i); ec.println(F(", 'off');\"/>OFF</state>"));
        break;
      case 'p':
        ec.print(F("      <state class=\"active\" onclick=\"xcJSON('/pulse/"));     ec.print(i); ec.println(F("');\"/>PULSE</state>"));
        break;
      case 'b':
        ec.print(F("      <state class=\"active\" onclick=\"xcJSON('/buzz/"));      ec.print(i); ec.println(F("');\"/>BUZZ</state>"));
        break;
    }
    ec.println(F("    </action>"));
    ec.println(F("  </card>"));
  }
}

// js file
void htmlJSCustom(EthernetClient ec) {
  httpHeader(ec, "application/javascript");
  ec.println(F("function xcRelaySwitch(r, s) {"));
  ec.println(F("  var s2=(s=='on'?'off':'on');"));
  ec.println(F("  var el=xcFind('#state'+r+s2);"));
  ec.println(F("  xcRemoveClass(el,'active');"));
  ec.println(F("  xcJSON('/switch/'+s+'/'+r, function(){var el=xcFind('#state'+r+s);xcAddClass(el,'active');});"));
  ec.println(F("}\n"));
}



////////////////////////////////////////////////////////////////////////////////
// Common HTTP and HTML functions for RELAY/LED/TEMP/TIME -Control sketches
////////////////////////////////////////////////////////////////////////////////

// handle basic request received with http
void interruptHttp() {
  boolean currentBlank;
  String currentLine;
  String currentURL;
  String currentReq;
  char c;
  EthernetClient client = netServer.available();
  if (client) {
    currentBlank = true;
    currentLine  = "";
    currentURL   = "";
    //  currentReq   = "";
    while (client.connected()) {
      if (client.available()) {
        c = client.read();
        //      currentReq.concat(c);
        if (c == '\n' && currentBlank) {
          //          httpProcess(client, currentURL, currentReq);
          httpProcess(client, currentURL);
          break;
        }
        if (c == '\n' or c == '\r') {
          currentBlank = true;
          if (currentLine.indexOf('GET') > 0) {
            currentURL = currentLine;
            currentURL.replace("GET", "");
            currentURL.replace("HTTP/1.1", "");
            currentURL.replace(" ", "");
            currentLine = "";
          }
        } else if (c != '\r') {
          currentBlank = false;
          currentLine.concat(c);
        }
      }
    }
    delay(50);
    client.stop();
  }
}

// http response header
void httpHeader(EthernetClient ec, String ct) {
  ec.println(F("HTTP/1.1 200 OK"));
  ec.print  (F("Content-Type: "));
  ec.println(ct);
  ec.println(F("Connection: close"));
  ec.println();
}

// html header fields and start html body
void htmlHeader(EthernetClient ec) {
  httpHeader(ec, "text/html");
  ec.println(F("<!DOCTYPE html>"));
  ec.println(F("<html>"));
  ec.println(F("<head>"));
  ec.println(F("<meta name=\"viewport\" content=\"width=500, initial-scale=1\">"));
  ec.println(F("<meta charset=\"utf-8\"/>"));
  ec.println(F("<link rel=\"stylesheet\" type=\"text/css\" href=\"/css/xcontrol.css\">"));
  ec.println(F("<link rel=\"stylesheet\" type=\"text/css\" href=\"//fonts.googleapis.com/css?family=Roboto&amp;subset=latin,latin-ext\">"));
  ec.println(F("<script src=\"/js/common\"></script>"));
  ec.println(F("<script src=\"/js/custom\"></script>"));
  ec.print(F("<title>"));
  ec.print(XCONTROL_NAME);
  ec.println(F("</title>"));
  ec.println(F("</head>"));
  ec.println(F("<body>"));
}

// html footer and ending
void htmlFooter(EthernetClient ec) {
  ec.println(F("</body>"));
  ec.println(F("</html>"));
}

// html menu
void htmlMenu(EthernetClient ec) {
  ec.println(F("<menu>"));
  for (byte i = 0; i < menuCount; i++) {
    ec.print(F("<a href=\""));
    ec.print(menuLinks[i]);
    ec.print(F("\">"));
    ec.print(menuNames[i]);
    ec.println(F("</a>"));
  }
  ec.println(F("</menu>"));
  ec.println(F("<subm>"));
  ec.println(F("<a href=\"#\">"));
  ec.print(XCONTROL_NAME);
  ec.println(F("</a>"));
  ec.println(F("<a class=\"refresh\" href=\"/\">&olarr;</a>"));
  ec.println(F("</subm>"));
}

// css file
void htmlCSS(EthernetClient ec) {
  httpHeader(ec, "text/css");
  ec.println(F("* {font-family:Roboto,Helvetica,Arial,sans-serif;font-weight:normal;margin:0;padding:0;}"));
  ec.println(F("body {background-color:#eee;}"));
  ec.println(F("menu {width:100%;height:80px;background-color:#3f51b5;text-align:center;}"));
  ec.println(F("menu a {display:inline-block;color:white;line-height:40px;text-decoration:none;margin:0 20px;}"));
  ec.println(F("menu a:hover {border-bottom:solid 3px white;line-height:34px;}"));
  ec.println(F("subm {padding:0 20px;height:80px;background-color:#9fa8da;margin-top:-40px !important;margin-bottom:30px !important;}"));
  ec.println(F("subm a {display:inline-block;height:40px;line-height:40px;color:white;text-decoration:none;margin-right:20px;}"));
  ec.println(F("subm a.refresh {float:right;color:white;background-color:#3f51b5;width:40px;height:40px;line-height:40px;margin-top:59px;margin-right:0;text-align:center;border-radius:50%;}"));
  ec.println(F("card,subm {display:block;width:100%;max-width:500px;margin:10px auto;font-size:1.2em;border-radius:2px;box-shadow:0 10px 36px 0 rgba(0,0,0,0.2);}"));
  ec.println(F("card {padding:20px;background-color:white;color:#3f51b5;}"));
  ec.println(F("name {display:inline-block;width:100%;}"));
  ec.println(F("desc {display:inline-block;width:100%;font-size:0.8em;color:black;margin-top:10px;}"));
  ec.println(F("action {display:block;width:100%;height:36px;margin-top:20px;font-size:0.8em;line-height:40px;color:#c0c0c0;cursor:pointer;}"));
  ec.println(F("state,select {display:inline-block;font-size:0.8em;height:36px;line-height:36px;margin-left:20px;float:right;border-radius:2px;}"));
  ec.println(F("state {padding:0 20px;text-align:center;color:#9fa8da;background-color:white;transition:box-shadow 0.3s cubic-bezier(0.4,0,0.2,1)}"));
  ec.println(F("state:hover {box-shadow: 0px 4px 8px 0px rgba(0,0,0,0.4);}"));
  ec.println(F("state.active {color:white;background-color:#9fa8da;}"));
}

// js file
void htmlJSCommon(EthernetClient ec) {
  httpHeader(ec, "application/javascript");

  ec.println(F("function xcJSON(url, hfunc) {"));
  ec.println(F("var r = new XMLHttpRequest();"));
  ec.println(F("r.open('GET', url, true);"));
  ec.println(F("r.onload = function() {"));
  ec.println(F("if (r.status >= 200 && r.status < 400) {"));
  ec.println(F("hfunc();"));
  ec.println(F("}"));
  ec.println(F("};"));
  ec.println(F("r.send();"));
  ec.println(F("}\n"));

  ec.println(F("function xcAddClass(el, cn) {"));
  ec.println(F("if (el.classList) el.classList.add(cn); else el.className += ' ' + cn;"));
  ec.println(F("}\n"));

  ec.println(F("function xcRemoveClass(el, cn) {"));
  ec.println(F("if (el.classList) el.classList.remove(cn); else el.className = el.className.replace(new RegExp('(^|\\b)' + cn.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');"));
  ec.println(F("}\n"));

  ec.println(F("function xcFind(sel) {"));
  ec.println(F("return document.querySelector(sel);"));
  ec.println(F("}\n"));
}

// json response
void jsonResponse(EthernetClient ec, byte id, String stat) {
  httpHeader(ec, "application/json");
  ec.print(F("{\"status\":\""));
  ec.print(stat);
  ec.print(F("\",\"id\":\""));
  ec.print(id);
  ec.println(F("\"}"));
}

