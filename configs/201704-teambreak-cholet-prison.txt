////////////////////////////////////////////////////////////////////////////////
// Circuit setup
////////////////////////////////////////////////////////////////////////////////

// relay configuration
const byte relayMode  PROGMEM    = 1;      // (1=GPIO pins; 2=Shift register)
const byte relayCount PROGMEM    = 8;      // number of relay pins used
const byte relayPins[]           = {       // GPIO pins used to control relays
  A0, 3, 4, 5, 6, 7, 8, 9
};
const byte relayShiftD PROGMEM   = 5;      // Shift register data pin
const byte relayShiftC PROGMEM   = 4;      // Shift register clock pin
const byte relayShiftL PROGMEM   = 3;      // Shift register latch pin
const char* relayNames[]         = {       // relay display naming
  "Cellule #215",
  "Cellule #216",
  "Cellule #217",
  "Tiroir de bureau",
  "Infirmary ext",
  "Infirmary int",
  "Countdown timer",
  "Iris scanner"
};
const char relayType[]           = {       // relay function type
  's', 's', 's', 's',
  's', 's', 's', 's',
};
boolean const relayInvert        = true;
int const     relayPulseLength   = 500;    // pulse length in milliseconds
int const     relayBuzzLength    = 80;     // buzz length in milliseconds
int const     relayBuzzCount     = 20;     // buzz number of periods

// menu configuration
const byte menuCount PROGMEM     = 1;      // number of menu items
const String menuNames[]         = {       // menu display naming
  "Prison #1"
};
const String menuLinks[]         = {       // menu href links
  "http://192.168.42.179"
};

// network configuration
byte netMAC[]                    = {       // mac address
  0xDE, 0xAD, 0xB0,
  0xEF, 0xFD, 0x10
};
byte netIP[]                     = {       // ip address
  192, 168, 42, 179
};