////////////////////////////////////////////////////////////////////////////////
// Circuit setup
////////////////////////////////////////////////////////////////////////////////

// relay configuration
const byte relayCount PROGMEM    = 7;      // number of relay pins used
const byte relayPins[]           = {       // io pins used to control relays
  9, 8, 6, 5, 3, A0, 4
//  9, 8, 7, 6, 5, 4, 3, A0
};
const char* relayNames[]         = {       // relay display naming
  "Gate #1",
  "Gate #2",
  "Gate #3",
  "Security camera",
  "Alien intelligence test",
  "Human-chain enable",
  "Comminicator transmit"
};
const char relayType[]           = {       // relay function type
  's', 's', 's', 's',
  's', 's', 's', 's',
};
boolean const relayInvert        = true;
int const     relayPulseLength   = 1000;   // pulse length in milliseconds
int const     relayBuzzLength    = 80;     // buzz length in milliseconds
int const     relayBuzzCount     = 20;     // buzz number of periods


// menu configuration
const byte menuCount PROGMEM     = 1;      // number of menu items
const String menuNames[]         = {       // menu display naming
  "Alien control"
};
const String menuLinks[]         = {       // menu href links
  "http://192.168.2.156"
};

// network configuration
byte netMAC[]                    = {       // mac address
  0xDE, 0xAD, 0xB0,
  0xEF, 0xFD, 0x04
};
byte netIP[]                     = {       // ip address
  192, 168, 2, 156
};