////////////////////////////////////////////////////////////////////////////////
// Circuit setup
////////////////////////////////////////////////////////////////////////////////

// relay configuration
const byte relayCount PROGMEM    = 7;      // number of relay pins used
const byte relayPins[]           = {       // io pins used to control relays
  A0, 3, 4, 5, 6, 7, 8, 9
};
const char* relayNames[]         = {       // relay display naming
  "Cellule #215",
  "Cellule #217",
  "Tiroir de bureau",
  "Infirmary ext",
  "Infirmary int",
  "Countdown timer",
  "Iris scanner"
};
const char relayType[]           = {       // relay function type
  's', 's', 's', 's',
  's', 's', 's', 's',
};
boolean const relayInvert        = true;
int const     relayPulseLength   = 1000;   // pulse length in milliseconds
int const     relayBuzzLength    = 80;     // buzz length in milliseconds
int const     relayBuzzCount     = 20;     // buzz number of periods

// menu configuration
const byte menuCount PROGMEM     = 1;      // number of menu items
const String menuNames[]         = {       // menu display naming
  "Home relay"
};
const String menuLinks[]         = {       // menu href links
  "http://192.168.33.150"
};

// network configuration
byte netMAC[]                    = {       // mac address
  0xDE, 0xAD, 0xB0,
  0xEF, 0xFD, 0x07
};
byte netIP[]                     = {       // ip address
  192, 168, 33, 150
};